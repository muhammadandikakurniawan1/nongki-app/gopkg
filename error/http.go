package error

type HttpErrorType int

func GetErrorType(errCode int) HttpErrorType {
	switch {
	case errCode >= 200 && errCode <= 299:
		return HTTP_SUCCESS
	case errCode >= 400 && errCode <= 499:
		return HTTP_INVALID_REQUEST
	case errCode >= 500 && errCode <= 599:
		return HTTP_INTERNAL_SERVER_ERROR
	default:
		return -1
	}
}

const (
	HTTP_SUCCESS HttpErrorType = iota
	HTTP_INTERNAL_SERVER_ERROR
	HTTP_INVALID_REQUEST
)
