package error

type ErrorCode interface {
	GetHttpStatus() int
	GetStatusCode() string
}
