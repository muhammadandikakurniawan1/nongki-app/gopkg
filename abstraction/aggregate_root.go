package abstraction

type Aggregate[T_ROOT any] interface {
	GetAggregateRoot() T_ROOT
	GetEvents() []AggregateEvent
}

func NewBaseAggregate[T_ROOT any](root T_ROOT) BaseAggregate[T_ROOT] {
	return BaseAggregate[T_ROOT]{
		aggreagateRoot: root,
		events:         []AggregateEvent{},
	}
}

type BaseAggregate[T_ROOT any] struct {
	aggreagateRoot T_ROOT
	events         []AggregateEvent
}

// func (o BaseAggregate[T_ROOT]) GetAggregateRoot() T_ROOT {
// 	return o.aggreagateRoot
// }

func (o BaseAggregate[T_ROOT]) GetEvents() []AggregateEvent {
	return o.events
}

func (o *BaseAggregate[T_ROOT]) AddEvent(e AggregateEvent) {
	o.events = append(o.events, e)
}
