package abstraction

import (
	"context"
	sharedErr "gitlab.com/muhammadandikakurniawan1/nongki-app/gopkg/error"
)

type UsecaseHandlerFunc[T_PARAM any, T_RESULT any] func(ctx context.Context, param T_PARAM) (result T_RESULT, err sharedErr.AppError)

type UsecaseHandler[T_PARAM any, T_RESULT any] interface {
	Execute(ctx context.Context, param T_PARAM) (result T_RESULT, err *sharedErr.AppError)
}
