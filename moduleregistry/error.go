package moduleregistry

import "net/http"

type errorCode string

func (e errorCode) GetHttpStatus() int {
	switch e {
	case ERROR_MODULE_NOTFOUND:
		return http.StatusNotFound
	case ERROR_INVALID_PARAMETER:
		return http.StatusBadRequest
	case ERROR_MODULE_ALREADY_EXISTS:
		return http.StatusBadRequest
	}
	return http.StatusInternalServerError
}

func (e errorCode) GetStatusCode() string {
	return string(e)
}

func (e errorCode) GetValidationErrorMessage() interface{} {
	return nil
}

const (
	ERROR_MODULE_NOTFOUND       errorCode = errorCode("MOD404")
	ERROR_INVALID_PARAMETER     errorCode = errorCode("MOD4001")
	ERROR_MODULE_ALREADY_EXISTS errorCode = errorCode("MOD4002")
)
