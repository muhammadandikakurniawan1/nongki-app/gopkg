package moduleregistry

import (
	"context"
	"encoding/json"
	"fmt"
	sharedErr "gitlab.com/muhammadandikakurniawan1/nongki-app/gopkg/error"
)

func NewModuleRegistry() ModuleRegistry {
	return &moduleRegistryImpl{
		modules: map[string]func(ctx context.Context, param any) (any, *sharedErr.AppError){},
	}
}

type ModuleRegistry interface {
	RegisterModule(path string, actModule func(ctx context.Context, param any) (any, *sharedErr.AppError)) (err *sharedErr.AppError)
	CallModule(path string, ctx context.Context, param any) (res any, err *sharedErr.AppError)
	IsModuleExists(path string) bool
}

type moduleRegistryImpl struct {
	modules map[string]func(ctx context.Context, param any) (any, *sharedErr.AppError)
}

func (m *moduleRegistryImpl) IsModuleExists(path string) bool {
	_, exists := m.modules[path]
	return exists
}

func (m *moduleRegistryImpl) RegisterModule(path string, actModule func(ctx context.Context, param any) (any, *sharedErr.AppError)) (err *sharedErr.AppError) {

	if m.IsModuleExists(path) {
		msg := fmt.Sprintf("module with path %s already exists", path)
		err = sharedErr.NewAppError(ERROR_MODULE_ALREADY_EXISTS, msg, msg)
	}

	m.modules[path] = actModule
	return
}

func (m *moduleRegistryImpl) CallModule(path string, ctx context.Context, param any) (res any, err *sharedErr.AppError) {
	act, exists := m.modules[path]
	if !exists {
		err = sharedErr.NewAppError(ERROR_MODULE_NOTFOUND, "module not found", "module not found")
		return
	}

	return act(ctx, param)
}

func RegisterSharedModule[T_PARAM any, T_RESULT any](registry ModuleRegistry, path string, act func(ctx context.Context, param T_PARAM) (T_RESULT, *sharedErr.AppError)) {

	regAct := func(ctx context.Context, param any) (result any, err *sharedErr.AppError) {
		paramParse, valid := param.(T_PARAM)
		if !valid {

			paramBytes, jsonErr := json.Marshal(param)
			if jsonErr != nil {
				err = sharedErr.NewAppError(ERROR_INVALID_PARAMETER, "invalid parameter", jsonErr.Error())
				return
			}

			if jsonErr = json.Unmarshal(paramBytes, &paramParse); jsonErr != nil {
				err = sharedErr.NewAppError(ERROR_INVALID_PARAMETER, "invalid parameter", jsonErr.Error())
				return
			}
		}

		result, err = act(ctx, paramParse)
		return
	}
	registry.RegisterModule(path, regAct)
}
