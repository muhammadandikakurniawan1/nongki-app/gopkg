package model

type User interface {
	GetUserId() string
	GetFullname() string
	GetEmail() string
}
