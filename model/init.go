package model

import "net/http"

func NewOkResponseModel[T_DATA any](data T_DATA, msg, StatusCode string) BaseResponseModel[T_DATA] {
	return BaseResponseModel[T_DATA]{
		Success:        true,
		Message:        msg,
		StatusCode:     StatusCode,
		HttpStatusCode: http.StatusOK,
		Data:           data,
	}
}
