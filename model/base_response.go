package model

import (
	sharedErr "gitlab.com/muhammadandikakurniawan1/nongki-app/gopkg/error"
)

type BaseResponseModel[T_DATA any] struct {
	Data                  T_DATA      `json:"data"`
	Message               string      `json:"message,omitempty"`
	ErrorMessage          string      `json:"error_message,omitempty"`
	Success               bool        `json:"success,omitempty"`
	StatusCode            string      `json:"status_code,omitempty"`
	HttpStatusCode        int         `json:"http_status_code,omitempty"`
	ValidationErrorMesage interface{} `json:"validation_error_message,omitempty"`
	PaginationResponseModel
}

func (m *BaseResponseModel[T_DATA]) SetData(data T_DATA) *BaseResponseModel[T_DATA] {
	m.Data = data
	return m
}

func (m *BaseResponseModel[T_DATA]) SetMessage(data string) *BaseResponseModel[T_DATA] {
	m.Message = data
	return m
}

func (m *BaseResponseModel[T_DATA]) SetErrorMessage(data string) *BaseResponseModel[T_DATA] {
	m.ErrorMessage = data
	return m
}

func (m *BaseResponseModel[T_DATA]) SetSuccess(data bool) *BaseResponseModel[T_DATA] {
	m.Success = data
	return m
}

func (m *BaseResponseModel[T_DATA]) SetStatusCode(data string) *BaseResponseModel[T_DATA] {
	m.StatusCode = data
	return m
}

func (m *BaseResponseModel[T_DATA]) SetHttpStatusCode(data int) *BaseResponseModel[T_DATA] {
	m.HttpStatusCode = data
	return m
}

func (m *BaseResponseModel[T_DATA]) SetError(err sharedErr.AppError) *BaseResponseModel[T_DATA] {
	m.Success = false
	m.Message = err.Message
	m.ErrorMessage = err.SystemErrorMessage
	m.StatusCode = err.GetErrorCode().GetStatusCode()
	m.HttpStatusCode = err.ErrorCode.GetHttpStatus()
	return m
}
