package asyncutil

type Act func(requestId string) error

func NewActRequestIdStorage(requestId string, act Act) ActRequestIdStorage {
	return ActRequestIdStorage{
		RequestId: requestId,
		Act:       act,
	}
}

type ActRequestIdStorage struct {
	RequestId string
	Act       Act
}
