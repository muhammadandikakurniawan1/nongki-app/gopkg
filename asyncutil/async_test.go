package asyncutil

import (
	"fmt"
	"testing"
	"time"
)

func TestAsyncUtil(t *testing.T) {
	len := int64(500)
	util := NewAsyncUtil(10, len)
	for i := int64(1); i <= len; i++ {
		x := i
		util.Do(func(requestId string) error {
			fmt.Printf("data - %d | req-id : %s\n", x, requestId)
			time.Sleep(5 * time.Second)
			return nil
		})
	}

	util.Wait()

	fmt.Println("=========== FINISH =============")
}
