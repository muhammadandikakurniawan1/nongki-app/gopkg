package asyncutil

import (
	"sync"

	"github.com/google/uuid"
)

func runActQueue(chanActQueue chan bool, mutex, mutexWorkerDoneCount *sync.Mutex, asyncWorkerCount int64, listAction *[]ActRequestIdStorage, currentAsyncWorkerDoneCount *int64, mapErrorByRequestId *map[string]error) {

	for range chanActQueue {
		go func() {
			mutex.Lock()
			var actData ActRequestIdStorage
			lenAct := len(*listAction)
			if lenAct > 0 {
				actData = (*listAction)[0]
				if lenAct > 1 {
					*listAction = (*listAction)[1:]
				} else {
					*listAction = []ActRequestIdStorage{}
				}
			}
			mutex.Unlock()
			if actData.RequestId != "" {
				if err := actData.Act(actData.RequestId); err != nil {
					(*mapErrorByRequestId)[actData.RequestId] = err
				}
				mutexWorkerDoneCount.Lock()
				*currentAsyncWorkerDoneCount = *currentAsyncWorkerDoneCount - 1
				chanActQueue <- true
				mutexWorkerDoneCount.Unlock()
			}
		}()
	}
}

func NewAsyncUtil(asyncWorkerCount, numberOfAction int64) AsyncUtil {
	var (
		currentAsyncWorkerCount     = int64(0)
		currentAsyncWorkerDoneCount = int64(numberOfAction)
		listAction                  = []ActRequestIdStorage{}
		mapErrorByRequestId         = map[string]error{}
		chanActQueue                = make(chan bool, asyncWorkerCount)
		mutex                       = sync.Mutex{}
		mutexWorkerDoneCount        = sync.Mutex{}
	)

	go runActQueue(chanActQueue, &mutex, &mutexWorkerDoneCount, asyncWorkerCount, &listAction, &currentAsyncWorkerDoneCount, &mapErrorByRequestId)

	return AsyncUtil{
		currentAsyncWorkerCount:     &currentAsyncWorkerCount,
		currentAsyncWorkerDoneCount: &currentAsyncWorkerDoneCount,
		listAction:                  &listAction,
		mapErrorByRequestId:         &mapErrorByRequestId,
		chanActQueue:                chanActQueue,
		asyncWorkerCount:            asyncWorkerCount,
		mutex:                       &mutex,
		mutexWorkerDoneCount:        &mutexWorkerDoneCount,
	}
}

type AsyncUtil struct {
	currentAsyncWorkerCount     *int64
	currentAsyncWorkerDoneCount *int64
	listAction                  *[]ActRequestIdStorage
	mapErrorByRequestId         *map[string]error
	chanActQueue                chan bool
	asyncWorkerCount            int64
	mutex, mutexWorkerDoneCount *sync.Mutex
}

func (a *AsyncUtil) GetListError() map[string]error {
	return *a.mapErrorByRequestId
}

func (a *AsyncUtil) addError(requestId string, err error) {
	(*a.mapErrorByRequestId)[requestId] = err
}

func (a *AsyncUtil) Wait() {
	chanDone := make(chan bool)
	go func() {
		for {
			if *a.currentAsyncWorkerDoneCount == 0 {
				close(a.chanActQueue)
				chanDone <- true
				return
			}
		}
	}()

	<-chanDone
	return
}

func (a *AsyncUtil) addCurrentTaskCount(requestId string, i int64) {
	a.mutex.Lock()
	count := *a.currentAsyncWorkerCount + i
	if count >= 0 {
		*a.currentAsyncWorkerCount = count
	}
	a.mutex.Unlock()
}

func (a *AsyncUtil) Do(act Act) {
	requestId := uuid.NewString()

	if *a.currentAsyncWorkerCount >= a.asyncWorkerCount {
		a.mutex.Lock()
		*a.listAction = append(*a.listAction, NewActRequestIdStorage(requestId, act))
		a.mutex.Unlock()
		return
	}

	a.addCurrentTaskCount(requestId, 1)

	go func() {
		if err := act(requestId); err != nil {
			a.addError(requestId, err)
		}
		a.mutexWorkerDoneCount.Lock()
		*a.currentAsyncWorkerDoneCount = *a.currentAsyncWorkerDoneCount - 1
		a.chanActQueue <- true
		a.mutexWorkerDoneCount.Unlock()
	}()
}
