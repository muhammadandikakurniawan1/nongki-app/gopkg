package cache

import "context"

type CacheStorage interface {
	SaveKeyVal(ctx context.Context, key string, val interface{}) (err error)
}
