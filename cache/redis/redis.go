package redis

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
	"gitlab.com/muhammadandikakurniawan1/nongki-app/gopkg/cache"
)

func New(cfg redis.Options) (res cache.CacheStorage, err error) {

	client := redis.NewClient(&cfg)

	pong, err := client.Ping(context.Background()).Result()
	if err != nil {
		fmt.Println("Failed to connect to Redis:", err)
		return
	}
	fmt.Println("Success to connect to Redis:", pong)

	res = &Redis{
		client: client,
	}
	return
}

type Redis struct {
	client *redis.Client
}

func (r Redis) SaveKeyVal(ctx context.Context, key string, val interface{}) (err error) {
	// Set a key-value pair in Redis
	err = r.client.Set(ctx, key, val, 0).Err()
	return
}
