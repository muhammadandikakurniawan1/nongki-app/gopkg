package deliveryutil

import (
	"encoding/json"
	"log"
	"net/http"

	sharedError "gitlab.com/muhammadandikakurniawan1/nongki-app/gopkg/error"
	"gitlab.com/muhammadandikakurniawan1/nongki-app/gopkg/model"
)

func Log(data interface{}, r *http.Request) {
	dataBytes, err := json.Marshal(data)
	if err != nil {
		log.Printf("error : %v\n", err)
		return
	}
	log.Printf("response %s : %s\n", r.URL.Path, string(dataBytes))
}

func ResponseJson[T any](w http.ResponseWriter, r *http.Request, model model.BaseResponseModel[T]) {
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(model.HttpStatusCode)
	Log(model, r)
	json.NewEncoder(w).Encode(model)
}

func ResponseErrorJson(w http.ResponseWriter, r *http.Request, err sharedError.AppError) {
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(err.ErrorCode.GetHttpStatus())
	model := model.BaseResponseModel[bool]{
		ErrorMessage:   err.Error(),
		Message:        err.Message,
		Success:        false,
		HttpStatusCode: err.ErrorCode.GetHttpStatus(),
		StatusCode:     err.GetErrorCode().GetStatusCode(),
	}
	if err.ValidationErrorMessage != nil {
		model.ValidationErrorMesage = err.ValidationErrorMessage
	}
	Log(model, r)
	json.NewEncoder(w).Encode(model)
}

func ReadRequestBody(w http.ResponseWriter, r *http.Request, dst interface{}) (err error) {
	err = json.NewDecoder(r.Body).Decode(dst)
	return
}
