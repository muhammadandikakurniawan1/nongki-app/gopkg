package messagebroker

import "context"

type ConsumerHandler func(ctx context.Context, message []byte, done chan<- bool) (err error)

type Client interface {
	Consume(ctx context.Context, config ConsumerConfig, handler ConsumerHandler, onError func(err error)) (err error)
	Publish(ctx context.Context, config PublisherConfig) (err error)
	Close() (err error)
}
