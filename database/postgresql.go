package database

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func NewPostgreSQL(cfg DatabaseConfig, config gorm.Config) (*gorm.DB, error) {
	connString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s %s", cfg.Host, cfg.Port, cfg.Username, cfg.Password, cfg.DBName, cfg.Parameter)
	db, err := gorm.Open(postgres.Open(connString), &config)
	if err != nil {
		return nil, err
	}
	return db, nil
}
