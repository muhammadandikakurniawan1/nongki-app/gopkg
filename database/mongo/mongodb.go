package mongo

import (
	"context"
	"fmt"
	"gitlab.com/muhammadandikakurniawan1/nongki-app/gopkg/database"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func NewConnection(cfg database.DatabaseConfig) (client *mongo.Client, err error) {
	conStrt := "mongodb://"
	if cfg.Username != "" && cfg.Password != "" {
		conStrt = fmt.Sprintf("%s%s:%s@", conStrt, cfg.Username, cfg.Password)
	}
	conStrt = fmt.Sprintf("%s%s:%d", conStrt, cfg.Host, cfg.Port)
	if cfg.Parameter != "" {
		conStrt = conStrt + "/?" + cfg.Parameter
	}

	client, err = mongo.NewClient(options.Client().ApplyURI(conStrt))
	if err != nil {
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	return
}

func NewConnectionByConnectionString(connStr string) (client *mongo.Client, err error) {

	client, err = mongo.NewClient(options.Client().ApplyURI(connStr))
	if err != nil {
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	return
}
