package database

type DatabaseConfig struct {
	Host      string
	Port      uint
	Username  string
	Password  string
	DBName    string
	Parameter string
}
