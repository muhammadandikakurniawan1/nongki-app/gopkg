package validationutility

import "strings"

func IsEmptyString(str string) bool {
	tempStr := strings.ReplaceAll(str, " ", "")
	return tempStr == ""
}
